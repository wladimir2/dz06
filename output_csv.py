import psycopg2

try:
 conn = psycopg2.connect(host="localhost",port="6789",database="shop_data", user="postgres")
 cur = conn.cursor()
 query = """ SELECT orders.order_id, customers.first_nm, goods.name, goods.vendor, order_items.quantity FROM (((customers INNER JOIN orders ON customers.cust_id=orders.cust_id) INNER JOIN order_items ON order_items.order_id=orders.order_id) INNER JOIN goods ON order_items.good_id=goods.good_id ) """
 outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)
 with open('dz06/orders.csv', 'w') as f:
    cur.copy_expert(outputquery, f)
 cur.close()
 conn.commit()
except (Exception, psycopg2.DatabaseError) as error:
 print(error)
finally:
 if conn is not None:
  conn.close()

