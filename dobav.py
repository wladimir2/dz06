import psycopg2

try:
 conn = psycopg2.connect(host="localhost", port="6789", database="shop_data", user="postgres")
 cur = conn.cursor()
 print("ДОБАВЛЕНИЕ товара в заказ")
 order = int(input("\n Введите id заказа: "))
 if order <= 0:
  raise Exception("Нет заказа с таким id")
 good  = int(input("\n Введите id товара: "))
 if good <= 0:
  raise Exception("Нет товара с таким id")
 cur.execute(" SELECT order_item_id FROM order_items WHERE order_id = %s AND good_id = %s",(str(order),str(good)))
 if cur.rowcount>0:
  raise Exception("Такой товар уже есть в заказе")
   #db_version = cur.fetchone()
 cur.execute(" INSERT INTO order_items(order_id,good_id,quantity) VALUES(%s,%s,%s)",(str(order),str(good),str(1)))
 cur.close()
 conn.commit()
except (Exception) as e:
 print("Ошибка: " + str(e))
finally:
 if conn is not None:
  conn.close()
