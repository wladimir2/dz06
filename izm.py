import psycopg2

try:
 conn = psycopg2.connect(host="localhost", port="6789", database="shop_data", user="postgres")
 cur = conn.cursor()
 print("ИЗМЕНЕНИЕ количества товара в заказе")
 order = int(input("\n Введите id заказа: "))
 if order <= 0:
  raise Exception("Нет заказа с таким id")
 good  = int(input("\n Введите id товара: "))
 if good <= 0:
  raise Exception("Нет товара с таким id")
 quan  = int(input("\n Введите необходимое количество товара в заказе: "))
 if quan <= 0:
  raise Exception("Слишком мало")

 cur.execute(" SELECT order_item_id FROM order_items WHERE order_id = %s AND good_id = %s",(str(order),str(good)))
 if cur.rowcount>0:
  cur.execute(" UPDATE order_items SET quantity=%s WHERE order_id = %s AND good_id = %s",(str(quan),str(order),str(good)))
 else:
  raise Exception("Такого товара нет в заказе. Добавьте товар в заказ с помощью dobav.py")
   #db_version = cur.fetchone()
 cur.close()
 conn.commit()
except (Exception) as e:
 print("Ошибка: " + str(e))
finally:
 if conn is not None:
  conn.close()
