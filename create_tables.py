import psycopg2

try:
 conn = psycopg2.connect(host="localhost", port="6789", database="shop_data", user="postgres")
 cur = conn.cursor()
 #cur.execute(""" CREATE TABLE vendors (vendor_id SERIAL PRIMARY KEY,vendor_name VARCHAR(255) NOT NULL) """)
 #cur.execute(" INSERT INTO vendors VALUES(1,'vendor1')")
 #cur.execute(" INSERT INTO vendors VALUES(2,'vendor2')")
 #cur.execute(" CREATE TABLE customers ( cust_id SERIAL PRIMARY KEY, first_nm VARCHAR(100) NOT NULL, last_nm VARCHAR(100) NOT NULL) ")
 #cur.execute(" INSERT INTO customers VALUES(1,'Ivan','Ivanov') ")
 #cur.execute(" INSERT INTO customers VALUES(2,'Petr','Petrov') ")
 #cur.execute(" CREATE TABLE orders ( order_id SERIAL PRIMARY KEY, cust_id INTEGER NOT NULL, order_dttm TIMESTAMP, status VARCHAR(20), FOREIGN KEY (cust_id) REFERENCES customers (cust_id) ON UPDATE CASCADE ON DELETE CASCADE) ")
 #cur.execute(" INSERT INTO orders VALUES(1,1,'2018-07-01 10:34:55','Ne gotov') ")
 #cur.execute(" INSERT INTO orders VALUES(2,2,'2018-07-02 11:34:55','Ne gotov') ")
 #cur.execute(" INSERT INTO orders VALUES(3,1,'2018-07-03 12:34:55','Ne gotov') ")
 #cur.execute(" CREATE TABLE goods ( good_id SERIAL PRIMARY KEY, vendor VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, description VARCHAR(300) NOT NULL ) ")
 #cur.execute(" INSERT INTO goods VALUES(1,'Adidas','Cap','Desc 1') ")
 #cur.execute(" INSERT INTO goods VALUES(2,'Nike','Boots','Desc 2') ")
 #cur.execute(" CREATE TABLE order_items ( order_item_id SERIAL PRIMARY KEY, order_id INTEGER NOT NULL, good_id INTEGER NOT NULL, quantity INTEGER NOT NULL, FOREIGN KEY (good_id) REFERENCES goods (good_id) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY (order_id) REFERENCES orders (order_id) ON UPDATE CASCADE ON DELETE CASCADE) ")
 #cur.execute(" INSERT INTO order_items VALUES(1,1,1,1) ")
 #cur.execute(" INSERT INTO order_items VALUES(2,2,2,2) ")
 #cur.execute(" INSERT INTO order_items VALUES(3,3,1,3) ")
 #cur.execute(" INSERT INTO order_items VALUES(4,3,2,4) ")

 cur.close()
 conn.commit()
except (Exception, psycopg2.DatabaseError) as error:
 print(error)
finally:
 if conn is not None:
  conn.close()
